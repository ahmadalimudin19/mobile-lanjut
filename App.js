import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'
import React, {useState} from 'react'

const Home = () => {
  const [nama1, setNama1] = useState('');
  const [nama2, setNama2] = useState('');
  const [fullName, setFullName] = useState('');

  const Hasil = () => {
       setFullName(`${nama1}${nama2}`)
  }
  return (
    <View style={styles.back}>
      <Text style={styles.text1}>MASUKAN NAMA</Text>
      <TextInput 
        style={styles.back1} 
        placeholder='Nama Depan'
        onChangeText={(val) => setNama1(val)}
      />
      <TextInput 
        style={styles.back1} 
        placeholder='Nama Belakang'
        onChangeText={(val) => setNama2(val)}
      />
      <View style={styles.wadah}>
        <TouchableOpacity style={styles.tombol} onPress={() => Hasil()}>
          <Text style={styles.text2}>Hasil</Text>
        </TouchableOpacity>
      </View>

      <View>
        <Text style={styles.text3}>
          {fullName?fullName : '...'}
        </Text>
      </View>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  text1:{
    fontWeight:'bold',
    alignSelf:'center',
    marginVertical:10,
  },
  back:{
    padding:10,
  },
  back1:{
    backgroundColor:'#F2F2F2',
    width:'100%',
    height: 40,
    alignItems:'center',
    borderRadius:5,
    paddingHorizontal:15,
    marginVertical:5,
  },
  tombol:{
    marginVertical:15,
    width:'40%',
    height:30,
    backgroundColor:'#00AD7C',
    borderRadius:5,
    alignItems:'center',
    justifyContent:'center',
  },
  wadah:{
    alignItems:'center',
  },
  text2:{
    color:'white',
    fontWeight:'bold',
    fontSize:15,
  },
  text3:{
    alignSelf:'center'
  }
})